#include "Helper.h"

// Remove whitespace from the begining and the end of the string
void Helper::trim(string &str) 
{
	rtrim(str);
	ltrim(str);

}

// Remove whitespace from the end of the string
void Helper::rtrim(string &str)
{
	size_t endpos = str.find_last_not_of(" \t");
	if (std::string::npos != endpos)
	{
		str = str.substr(0, endpos + 1);
	}
}

// Remove whitespace from the beginning of the string
void Helper::ltrim(string &str)
{
	size_t startpos = str.find_first_not_of(" \t");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

// Get a vector of strings, and return a vector of words 
// ("for example" vec[0]:"for" vec[1]:"example"
vector<string> Helper::get_words(string &str)
{
	vector<string> words;
	std::istringstream strStream(str);
	copy(istream_iterator<string>(strStream),
		istream_iterator<string>(),
		back_inserter(words));

	return words;
}
