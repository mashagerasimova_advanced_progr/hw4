#include <Windows.h>
#include <cstddef>
#include <iostream>
#include <string>
#include "Helper.h"

using namespace std;

void pwd();
void cd(string);
void create(string);
void ls(string);
void secret();
void exe(string);

#define SECRET_PATH "C:\\Users\\magshimim\\Desktop\\Secret.dll"
#define EXE 4

int main()
{
	Helper h;
	string command = "";
	while (true)
	{
		cout << "Insert command: ";
		getline(cin, command);
		vector<string> v = h.get_words(command); // Getting command from user
		if (v[0] == PWD) // If the 1st word is "pwd"
		{
			pwd(); // Calling the pwd func
		}
		else if (v[0] == CD) // If the 1st word is "cd"
		{
			cd(v[1]); // Calling the cd func and giving it the 2nd word as a parameter
		}
		else if (v[0] == CREATE) // If the 1st word is "create"
		{
			create(v[1]); // Calling the create func and giving it the 2nd word as a parameter
		}
		else if (v[0] == LS) // If the 1st word is "ls"
		{
			ls(v[1]); // Calling the ls func and giving it the 2nd word as a parameter
		}
		else if (v[0] == SECRET) // If the 1st word is "secret"
		{
			secret(); // Calling the secret func
		}
		else if (v[0].find(".exe")) // If the 1st word contains ".exe" substring
		{
			exe(v[0]); // Calling the exe func and giving it the 1st word as a parameter
		}
		else
		{
			cout << "Invalid command" << endl; // The command is invalid
		}
	}
	system("pause");
	return 0;
}

/*
The following func prints the current folder.
Input: -
Output: -
*/

void pwd()
{
	TCHAR path[PATH_MAX];
	DWORD  d = GetCurrentDirectory(PATH_MAX, path);
	if (d == 0)
	{
		cout << "error";
	}
	else
	{
		cout << path << endl;
	}
}

/*
The following function changes the current folder.
Input: path string to change
Output: -
*/

void cd(string path)
{
	if (SetCurrentDirectory(path.c_str()))
	{
		cout << "Successfull" << endl;
	}
	else
	{
		cout << "Unsuccessfull" << endl;
	}
}

/*
The following func creates new file
Input: path with file name
Output: -
*/

void create(string name)
{
	HANDLE hOut;
	hOut = CreateFile(name.c_str(), 0, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hOut == INVALID_HANDLE_VALUE) {
		cout << "ERROR " << GetLastError() << endl;
	}
	CloseHandle(hOut);
}

/*
The following func prints list of all the files in given folder
Input: direction string
Output: -
*/

void ls(string dir)
{

	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	TCHAR szDir[MAX_PATH];
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;

	StringCchCopy(szDir, MAX_PATH, dir.c_str());
	StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

	// Find the first file in the directory.

	hFind = FindFirstFile(szDir, &ffd);

	if (INVALID_HANDLE_VALUE == hFind)
	{
		cout << "Error" << endl;
	}

	// List all the files in the directory with some info about them.

	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) // If the file is hidden file
		{
			continue; // Don't print it
		}
		else // Another existing files
		{
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;
			cout << ffd.cFileName << endl;
		}
	} while (FindNextFile(hFind, &ffd) != 0); // Till the end

	dwError = GetLastError();
	if (dwError != ERROR_NO_MORE_FILES)
	{
		cout << "Error" << endl;
	}

	FindClose(hFind);
}

/*
The following func prints the output of TheAnswerToLifeTheUniverseAndEverything func in Secret.dll
Input: -
Output: -
*/

void secret()
{
	HINSTANCE hLib = LoadLibrary(SECRET_PATH); // Opening the dll

	if (!hLib)
	{
		cout << "Can't load library!" << endl;
	}
	else
	{
		FARPROC Func = (FARPROC)GetProcAddress((HMODULE)hLib, "TheAnswerToLifeTheUniverseAndEverything"); // Access to the required func in dll

		if (Func == NULL)
		{
			cout << "Can't load function." << endl;
		}
		else
		{
			cout << Func() << endl; // Printing the output of the func
		}
		
		FreeLibrary((HMODULE)hLib);
	}
}

/*
The following func opens an .exe file
Input: file name string
Output: -
*/

void exe(string file)
{
	if (file.rfind(".exe") == strlen(file.c_str()) - EXE) // Checking if the file name ends with ".exe"
	{
		TCHAR path[PATH_MAX];
		GetCurrentDirectory(PATH_MAX, path); // Getting the current folder
		string slashes = "\\";
		string fullPath = path + slashes + file; // Building the whole path together with the file name
		ShellExecute(NULL, "open", fullPath.c_str(), NULL, NULL, SW_SHOWDEFAULT); // Opening the file
	}
	else
	{
		cout << "Not .exe file" << endl;
	}

}

